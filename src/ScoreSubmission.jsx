import React from "react";
import { firestore } from "./firebase";

function ScoreSubmission({ user, score, onScoreSubmitted }) {
  const handleSubmit = (e) => {
    e.preventDefault();

    if (!user || !user.email) {
      alert("Please log in to submit your score.");
      return;
    }

    const leaderboardRef = firestore.collection("leaderboard");
    leaderboardRef
      .add({
        email: user.email,
        score,
      })
      .then(() => {
        alert("Score submitted successfully!");
        onScoreSubmitted();  // Call the prop function after successful submission
      })
      .catch((error) => {
        console.error("Error submitting score:", error);
        alert("Failed to submit score. Please try again.");
      });
  };

  return (
    <div>
      <h3>Submit Your Score</h3>
      <form onSubmit={handleSubmit}>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default ScoreSubmission;
