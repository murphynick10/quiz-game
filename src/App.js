import React, { useEffect, useState } from "react";
import { auth } from "./firebase";
import Quiz from "./Quiz";
import Authentication from "./Authentication";
import Leaderboard from "./Leaderboard";

function App() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      if (user) {
        setUser(user);
      } else {
        setUser(null);
      }
    });

    return () => {
      unsubscribe();
    };
  }, []);

  const handleLogout = () => {
    auth.signOut()
      .then(() => {
        console.log("User logged out");
      })
      .catch((error) => {
        console.error("Error logging out:", error);
      });
  };

  const onQuizCompleted = (score) => {
    console.log(`Quiz completed with score: ${score}`);
  };

  return (
    <div>
      <h1>Quiz Game</h1>
      {user ? (
        <>
          <Quiz user={user} onQuizCompleted={onQuizCompleted} />
          <button onClick={handleLogout}>Log Out</button>
          <Leaderboard />
        </>
      ) : (
        <Authentication />
      )}
    </div>
  );
}

export default App;
