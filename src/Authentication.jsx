import React, { useState } from "react";
import { auth } from "./firebase";

function Authentication() {
  const [signUpEmail, setSignUpEmail] = useState("");
  const [signUpPassword, setSignUpPassword] = useState("");
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const [error, setError] = useState("");

  const handleSignUp = () => {
    auth.createUserWithEmailAndPassword(signUpEmail, signUpPassword)
      .then((userCredential) => {
        const user = userCredential.user;
        console.log("User signed up:", user);
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  const handleLogin = () => {
    auth.signInWithEmailAndPassword(loginEmail, loginPassword)
      .then((userCredential) => {
        const user = userCredential.user;
        console.log("User logged in:", user);
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  const handleLogout = () => {
    auth.signOut()
      .then(() => {
        console.log("User logged out");
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  return (
    <div>
      <h3>Please sign up or log in to play the quiz.</h3>
      <div>
        <h4>Sign Up</h4>
        <input
          type="email"
          placeholder="Email"
          value={signUpEmail}
          onChange={(e) => setSignUpEmail(e.target.value)}
        />
        <input
          type="password"
          placeholder="Password"
          value={signUpPassword}
          onChange={(e) => setSignUpPassword(e.target.value)}
        />
        <button onClick={handleSignUp}>Sign Up</button>
      </div>
      <div>
        <h4>Log In</h4>
        <input
          type="email"
          placeholder="Email"
          value={loginEmail}
          onChange={(e) => setLoginEmail(e.target.value)}
        />
        <input
          type="password"
          placeholder="Password"
          value={loginPassword}
          onChange={(e) => setLoginPassword(e.target.value)}
        />
        <button onClick={handleLogin}>Log In</button>
      </div>
      {error && <p>Error: {error}</p>}
    </div>
  );
}

export default Authentication;
