import React, { useEffect, useState } from "react";
import { auth, firestore } from "./firebase";

function Quiz({ user, onQuizCompleted }) {
  const [questions, setQuestions] = useState([]);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [selectedOption, setSelectedOption] = useState("");
  const [score, setScore] = useState(0);

  useEffect(() => {
    fetchQuestions();
  }, []);

  useEffect(() => {
    if (currentQuestionIndex >= questions.length) {
      submitScore();
    }
  }, [currentQuestionIndex, questions.length]);

  const fetchQuestions = async () => {
    try {
      const questionsRef = firestore.collection("questions");
      const snapshot = await questionsRef.get();
      const fetchedQuestions = snapshot.docs.map((doc) => {
        const data = doc.data();
        return {
          ...data,
          correctAnswer: parseInt(data.correctAnswer),
        };
      });
      setQuestions(fetchedQuestions);
    } catch (error) {
      console.error("Error fetching questions:", error);
    }
  };

  const handleOptionSelect = (optionIndex) => {
    setSelectedOption(optionIndex.toString());
  };

  const handleNextQuestion = () => {
    const selectedOptionIndex = parseInt(selectedOption, 10);

    if (selectedOptionIndex === questions[currentQuestionIndex].correctAnswer) {
      setScore((prevScore) => prevScore + 1);
    }

    setSelectedOption("");
    setCurrentQuestionIndex((prevIndex) => prevIndex + 1);
  };

  const submitScore = () => {
    if (currentQuestionIndex === 0) {
      // No questions played yet, don't submit the score
      onQuizCompleted(0);
      return;
    }

    const scoreEntry = {
      username: user.displayName,
      email: user.email,
      score: score,
    };

    firestore
      .collection('leaderboard')
      .doc(user.email)
      .get()
      .then((docSnapshot) => {
        if (!docSnapshot.exists || score > docSnapshot.data().score) {
          firestore
            .collection('leaderboard')
            .doc(user.email)
            .set(scoreEntry)
            .then(() => {
              console.log("Score submitted!");
              onQuizCompleted(score);
            })
            .catch((error) => {
              console.error("Error writing score to Firestore: ", error);
            });
        } else {
          console.log("Score not submitted. Previous score is higher.");
          onQuizCompleted(docSnapshot.data().score);
        }
      })
      .catch((error) => {
        console.error("Error checking previous score in Firestore: ", error);
      });
  };

  const currentQuestion = questions[currentQuestionIndex];

  return (
    <div>
      {currentQuestionIndex < questions.length ? (
        <>
          <h3>Question {currentQuestionIndex + 1}</h3>
          <h4>{currentQuestion.question}</h4>
          <ul>
            {currentQuestion.options.map((option, index) => (
              <li key={index}>
                <label>
                  <input
                    type="radio"
                    name="option"
                    value={index}
                    checked={selectedOption === index.toString()}
                    onChange={() => handleOptionSelect(index)}
                  />
                  {option}
                </label>
              </li>
            ))}
          </ul>
          <button onClick={handleNextQuestion} disabled={!selectedOption}>
            Next
          </button>
        </>
      ) : (
        <>
          <h3>Quiz Completed!</h3>
          <p>
            Your score: {score}/{questions.length}
          </p>
          <button onClick={() => window.location.reload()}>Play Again</button>
        </>
      )}
    </div>
  );
}

export default Quiz;
