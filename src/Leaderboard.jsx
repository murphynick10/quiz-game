import React, { useEffect, useState } from "react";
import { firestore } from "./firebase";

function Leaderboard() {
  const [leaderboard, setLeaderboard] = useState([]);

  useEffect(() => {
    const leaderboardRef = firestore.collection("leaderboard");
    const unsubscribe = leaderboardRef
      .orderBy("score", "desc")
      .limit(10)
      .onSnapshot((snapshot) => {
        const updatedLeaderboard = snapshot.docs.map((doc) => doc.data());
        setLeaderboard(updatedLeaderboard);
      });

    return () => unsubscribe();
  }, []);

  return (
    <div>
      <h3>Leaderboard</h3>
      <ul>
        {leaderboard.map((entry, index) => (
          <li key={index}>
            {entry.email}: {entry.score}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Leaderboard;
